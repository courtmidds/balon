from django.contrib.gis.db import models
from django.utils import timezone
from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin



class Court(models.Model):
    """
    Model for basketball courts
    """
    STATUS_CHOICES = (
        ('slow', 'slow'),
        ('happening', 'game occuring'),
        ('popping', 'people inline waiting to play')
    )
    NET_CHOICES = (
        ('bare', 'no net'),
        ('chain', 'chain net'),
        ('nylon', 'nylon')
    )
    court_name = models.CharField(max_length=32)
    location = models.PointField()
    status = models.CharField(
        choice=STATUS_CHOICES,
        max_length=15,
        default='slow',
    )
    net = models.CharField(
        choice=NET_CHOICES,
        max_length=10,
        default='nylon',
    )
    has_single_rims = models.BooleanField(default=False)
    has_water_fountain = models.BooleanField(default=False)
    is_indoor = models.BooleanField(default=False)


class Player(models.Model):
    """
    Model for bball player
    """
    SKILL_CHOICES = (
        ('AAU', 'novice'),
        ('JV', 'intermediate'),
        ('College', 'decent'),
        ('NBA', 'cold')
    )
    name = models.CharField(max_length=20)
    location = models.PointField()
    email = models.CharField(max_length=50)
    skill_level = models.CharField(
        choices=SKILL_CHOICES,
        max_length=10,
        default='AAU'
    )
    has_ball = models.BooleanField(default=False)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    picture_file = models.CharField(max_length=64, default="", blank=True)
    has_profile_pic = models.BooleanField(default=False)


class PlayerInline(admin.StackedInline):
    model = Player
    can_delete = False


class UserAdmin(BaseUserAdmin):
    inlines = (PlayerInline,)


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
