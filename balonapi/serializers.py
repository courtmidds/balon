from rest_framework import serializers
from balonapi.models import *


class CourtSerializer(serializers.ModelSerializer):
    class Meta:
        model = Court
        fields = (
            'id',
            'court_name',
            'location',
            'status',
            'net',
            'has_single_rims',
            'has_water_fountain',
            'is_indoor'
        )


class PlayerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Player
        fields = (
            'id',
            'name',
            'email',
            'skill_level',
            'has_ball',
            'has picture',
            'picture_file',
            'location'
        )